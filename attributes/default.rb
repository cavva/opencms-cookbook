#
# Cookbook Name:: directory
# Attributes:: default

default['opencms']['base_document_path'] = '/store/www/opencms'
default['opencms']['localesconfigured'] = [ 'en', 'de' ]#
default['opencms']['localesdefault'] = [ 'en', 'de' ]#
default['opencms']['workplace-server'] = 'http://www.example.com'
default['opencms']['default-uri'] = '/sites/default/'
default['opencms']['shared-folder'] = '/shared/'
default['opencms']['scheduled-job'] = nil
default['opencms']['sites'] = [
  {
    'server'=>'http://www.example.com',
    'uri' =>'/sites/default/',
    'position' => nil,
    'title' => nil,
    'webserver' => nil
  }
]
default['opencms']['tool-manager'] = nil

=begin
default['opencms']['tool-manager'] = [
  {
    'key'=>'explorer',
    'uri' =>'/system/workplace/explorer/',
    'name' => '${key.GUI_EXPLORER_VIEW_ROOT_NAME_0}',
    'helptext' => '${key.GUI_EXPLORER_VIEW_ROOT_HELP_0}'
  }
]
=end

default['opencms']['file-suffix'] = [ '.js', '.woff']

default['opencms']['vfs-prefix'] = '${CONTEXT_NAME}${SERVLET_NAME}'

default['opencms']['schema.xml']['fieldType'] = nil
default['opencms']['schema.xml']['field'] = nil
default['opencms']['schema.xml']['dynamicField'] = nil
default['opencms']['schema.xml']['copyField'] = nil

case node['platform_family']
  when 'rhel', 'fedora'
  when 'debian'
  when 'smartos'
  else
end

