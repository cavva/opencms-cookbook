#
# Cookbook Name:: opencms
# Recipe:: default
#
# Copyright 2015, Davide Cavarretta
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

package 'unzip' do
  action :install
end

if !File.exists?("#{node['opencms']['base_document_path']}")
  remote_file '/tmp/opencms.zip' do
    source 'http://www.opencms.org/downloads/opencms/opencms-9.5.2.zip'
    owner 'root'
    group 'root'
    mode '0644'
    action :create
  end

  directory "#{node['opencms']['base_document_path']}" do
    owner "#{node['opencms']['user']}"
    group "#{node['opencms']['group']}"
    recursive true
    mode '0775'
    action :create
  end

  bash "extracting files" do
    cwd "#{node['opencms']['base_document_path']}/.."
    code <<-EOH
      unzip /tmp/opencms.zip
      chown #{node['opencms']['user']}:#{node['opencms']['group']} -R "opencms.war"
      EOH
  end

  directory "#{node['opencms']['base_document_path']}" do
    action :delete
  end

  service "tomcat-opencms" do
    action :restart
  end
  file "/tmp/opencms.zip" do
    action :delete
  end
end

directory "#{node['opencms']['base_document_path']}/WEB-INF/config/" do
  owner "#{node['opencms']['user']}"
  group "#{node['opencms']['group']}"
  recursive true
  mode '0775'
  action :create
end

template "#{node['opencms']['base_document_path']}/WEB-INF/config/opencms-importexport.xml" do
  source 'opencms-importexport.xml.erb'
  owner "#{node['opencms']['user']}"
  group "#{node['opencms']['group']}"
  mode '0644'
end

template "#{node['opencms']['base_document_path']}/WEB-INF/config/opencms-workplace.xml" do
  source 'opencms-workplace.xml.erb'
  owner "#{node['opencms']['user']}"
  group "#{node['opencms']['group']}"
  mode '0644'
end

template "#{node['opencms']['base_document_path']}/WEB-INF/config/opencms-system.xml" do
  source 'opencms-system.xml.erb'
  owner "#{node['opencms']['user']}"
  group "#{node['opencms']['group']}"
  mode '0644'
end

if File.exist?("#{node['opencms']['base_document_path']}/WEB-INF/solr/conf/") 
  template "#{node['opencms']['base_document_path']}/WEB-INF/solr/conf/schema.xml" do
    source 'schema.xml.erb'
    owner "#{node['opencms']['user']}"
    group "#{node['opencms']['group']}"
    mode '0644'
  end
end
